from math import gcd


def solve(a, b, m):
    d = gcd(a, m)
    if b % d:
        return
    if b > m:
        b = b % m

    if d != 1:
        a, b, m = a // d, b // d, m // d
    result = pow(a, phi(m) - 1) * b
    result %= m
    return result


def phi(n):
    amount = 0
    for k in range(1, n + 1):
        if gcd(n, k) == 1:
            amount += 1
    return amount
