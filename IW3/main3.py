import sys

from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import *
from RC5 import RC5

task = """№ 9. Реализовать программный продукт, позволяющий шифровать и
расшифровывать сообщения на русском языке с помощью RC5 для трех
различных вариантов. Чтение открытого текста и шифртекста должно быть
возможно с клавиатуры, запись результата шифрования/расшифрования на
экран. Ключ формируется автоматически и сохраняется на весь сеанс
шифрования. Ключ сохраняется в отдельный файл. Для реализации
криптоалгоритмов запрещено пользоваться встроенными библиотеками
используемых языков."""
CRYPT = 0
DECRYPT = 1


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.mainWidget = QWidget()
        self.setCentralWidget(self.mainWidget)
        self.source_text_widget = QLineEdit("текст который должен быть зашифрован")
        self.result_widget = QLineEdit()
        self.w_input_widget = QLineEdit("32")
        self.r_input_widget = QLineEdit("25")
        self.b_input_widget = QLineEdit("32")
        self.RC5 = None
        self.onlyInt = QIntValidator()
        self.initUI()

    def initUI(self):
        self.setWindowTitle("ИЗ 3: задача 9")
        grid = QGridLayout()

        self.w_input_widget.setValidator(self.onlyInt)
        self.r_input_widget.setValidator(self.onlyInt)
        self.b_input_widget.setValidator(self.onlyInt)
        self.source_text_widget.setMinimumWidth(300)
        self.result_widget.setReadOnly(True)

        button_encrypt = QPushButton("Шифровать")
        button_decrypt = QPushButton("Расшифровать")
        button_encrypt.clicked.connect(self.go_encrypt)
        button_decrypt.clicked.connect(self.go_decrypt)

        parameters = QWidget()
        parameters_layout = QHBoxLayout(parameters)
        parameters_layout.addWidget(QLabel("W:"))
        parameters_layout.addWidget(self.w_input_widget)
        parameters_layout.addWidget(QLabel("R:"))
        parameters_layout.addWidget(self.r_input_widget)
        parameters_layout.addWidget(QLabel("b:"))
        parameters_layout.addWidget(self.b_input_widget)

        self.mainWidget.setLayout(grid)
        grid.addWidget(parameters, 0, 0, 1, 3)
        grid.addWidget(QLabel("Исходный текст:"), 1, 0)
        grid.addWidget(self.source_text_widget, 1, 1)
        grid.addWidget(button_encrypt, 1, 2)
        grid.addWidget(button_decrypt, 2, 2)
        grid.addWidget(QLabel("Результат:"), 2, 0)
        grid.addWidget(self.result_widget, 2, 1)

    def getRC5(self, mode):
        key = None
        if self.RC5 is None:
            if mode == DECRYPT:
                key = self.get_key_from_file()
            if self.w_input_widget.text() and self.r_input_widget.text() and self.b_input_widget.text():
                if key is None:
                    self.RC5 = RC5(int(self.w_input_widget.text()), int(self.r_input_widget.text()),
                                   int(self.b_input_widget.text()))
                else:
                    self.RC5 = RC5(int(self.w_input_widget.text()), int(self.r_input_widget.text()),
                                   int(self.b_input_widget.text()), key)

                return True
            else:
                return False
        if self.RC5.w != int(self.w_input_widget.text()) or self.RC5.R != int(
                self.r_input_widget.text()) or self.RC5.b != int(self.b_input_widget.text()):
            self.RC5 = RC5(int(self.w_input_widget.text()), int(self.r_input_widget.text()),
                           int(self.b_input_widget.text()))
        return True

    def go_encrypt(self):
        if not self.preparation():
            return
        result = self.RC5.encryptBytes(self.source_text_widget.text().encode())
        self.result_widget.setText(result.decode('iso8859-1'))
        self.save_key()

    def save_key(self):
        with open("key.txt", "wb") as file:
            file.write(self.RC5.key)
        with open("result.txt", "wb") as file:
            file.write(self.result_widget.text().encode('iso8859-1'))

    def go_decrypt(self):
        if not self.preparation(mode=DECRYPT):
            return
        result = self.RC5.decryptBytes(self.source_text_widget.text().encode("iso8859-1"))
        self.result_widget.setText(result.decode())

    def preparation(self, mode=CRYPT):
        if not self.getRC5(mode):
            return
        if not self.source_text_widget.text().encode():
            return
        return True

    def get_key_from_file(self):
        # try:
        if not self.source_text_widget.text():
            with open("result.txt", "rb") as file:
                self.source_text_widget.setText(file.read().decode('iso8859-1'))
        with open("key.txt", "rb") as file:
            return file.read()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
