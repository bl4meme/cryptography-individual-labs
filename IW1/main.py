import sys
from math import gcd

from PyQt5.QtCore import *
from PyQt5.QtGui import QRegExpValidator, QFont, QTextCursor
from PyQt5.QtWidgets import *

task = """№ 7. Реализовать программный продукт решения сравнений первой степени
двумя способами с указанием всех промежуточных шагов вычисления
(текущее значение коэффициентов расширенном алгоритме Евклида и
текущее значение степеней в формуле Эйлера), программный продукт так же
должен реализовывать возможность того, что сравнение не имеет решений или
имеет больше одного решения. В первом случае сообщать пользователю с
пояснением, во втором строить все возможные решения."""


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.first = self.getInputByName("a")
        self.second = self.getInputByName("b")
        self.third = self.getInputByName("m")
        self.log_info = QPlainTextEdit()  # главное окно вывода информации о решении
        self.mainWidget = QWidget()
        self.setCentralWidget(self.mainWidget)  # main layout
        self.initUI()

    def initUI(self):
        # просто разметка
        self.setWindowTitle("ИЗ 1: задача 7")
        self.log_info.setReadOnly(True)
        self.log_info.setMinimumWidth(520)
        self.log_info.setFont(QFont('Times', 14))

        button_euclid = QPushButton("Эвклид")
        button_euler = QPushButton("Эйлер")
        button_delete = QPushButton("Очистить")
        button_euclid.clicked.connect(self.euclid)
        button_euler.clicked.connect(self.euler)
        button_delete.clicked.connect(self.clear)
        button_euclid.setMaximumWidth(80)
        button_euler.setMaximumWidth(80)
        button_delete.setMaximumWidth(80)

        main_layout = QHBoxLayout(self.mainWidget)
        left_widget = QWidget()
        left_layout = QVBoxLayout(left_widget)
        left_widget.setMaximumWidth(90)
        left_layout.addWidget(QLabel("ax ≡ b (mod m)"))

        left_layout.addWidget(self.first)
        left_layout.addWidget(self.second)
        left_layout.addWidget(self.third)

        left_layout.addWidget(button_euclid)
        left_layout.addWidget(button_euler)
        left_layout.addWidget(button_delete)

        main_layout.addWidget(left_widget)
        main_layout.addWidget(self.log_info)

    def euclid(self):
        # решение методом Эвклида
        simpl = self.simplification("Эвклида")
        if not simpl:
            return
        a, b, m, d = simpl
        x, y = self.dio_solve(a, m, b)
        result = x % m
        self.addToInfo(f'Решение диофантового уравнения x = {x} y = {y}')
        self.addToInfo(f'{x} % {m} = {result}')
        self.output_results(d, result, m)

    def euler(self):
        # решение методом Эйлера
        simpl = self.simplification("Эвклида")
        if not simpl:
            return
        a, b, m, d = simpl
        self.addToInfo(f'X = {a}^{self.phi(m) - 1} * {b}  mod {m}')
        result = pow(a, self.phi(m) - 1) * b
        result %= m
        self.output_results(d, result, m)

    def simplification(self, name):
        # базовые операции необходимые для решения сравнений, используеются в обоих методах, поэтому вынесены
        a, b, m = self.get_coefficients()
        if a is None:
            return
        self.addToInfo(f'Решение сравнение {a}x ≡ {b} mod {m} методом {name}')
        d = gcd(a, m)
        if b % d:
            self.addToInfo(f'b = {b} не делится на НОД({a}, {m}) = {d}. Решений нет!')
            return
        if b > m:
            self.addToInfo(f'Берём остаток от b = {b} по модулю {m} = {b % m}')
            b = b % m
            self.addToInfo(f'Полученное сравнение: {a}x ≡ {b} mod {m}')

        if d != 1:
            a, b, m = a // d, b // d, m // d
            self.addToInfo(f"Делим сравнение на {d}, получим {d} решений")
            self.addToInfo(f"Полученное сравнение: {a}x ≡ {b} mod {m}")
        return a, b, m, d

    def output_results(self, d, result, m):
        # процедура для вывода ответа(-ов)
        if d != 1:
            self.addToInfo(f"Промежуточный ответ = {result}")
            self.addToInfo(f'Ответы: {[result + m * i for i in range(d)]}\n\n')
        else:
            self.addToInfo(f'Ответ = {result}\n\n')

    def clear(self):
        # очистка логов
        self.log_info.setPlainText("")

    def get_coefficients(self):
        # получение введённой информации из полей ввода и проверка их корректности
        a = self.first.text()
        b = self.second.text()
        m = self.third.text()
        if a and b and m:
            return int(a), int(b), int(m)
        self.addToInfo("Введите все коэффициенты")
        return None, None, None

    def addToInfo(self, text):
        # вспомогательная процедура для дописывания информации в лог
        self.log_info.setPlainText(f'{self.log_info.toPlainText()} {text} \n')
        self.log_info.moveCursor(QTextCursor.End)

    def dio_solve(self, a, b, c):
        # функция для решения диофантовых уравнений
        if c % gcd(a, b) != 0:
            self.addToInfo("Решения нет.")
            return
        x, y = self.adv_gcd(a, b)
        return x * c, y * c

    def adv_gcd(self, a, b):
        # расширенный алгоритм Эвклида с вывоводом промежуточных коэф-ов
        if not b:
            return 1, 0
        y, x = self.adv_gcd(b, a % b)
        self.addToInfo(f"Промежуточные коэффициенты: x={x} y={y - (a // b) * x}")
        return x, y - (a // b) * x

    @staticmethod
    def phi(n):
        # возвращает значение функции Эйлера от заданного n
        amount = 0
        for k in range(1, n + 1):
            if gcd(n, k) == 1:
                amount += 1
        return amount

    @staticmethod
    def getInputByName(name):
        # вспомогательная функция для создания полей ввода
        reg_ex = QRegExp("[0-9]+")
        inputWidget = QLineEdit()
        inputWidget.setObjectName(name)
        inputWidget.setAlignment(Qt.AlignCenter)
        inputWidget.setPlaceholderText(name)
        inputWidget.setMaximumWidth(80)
        input_validator = QRegExpValidator(reg_ex, inputWidget)
        inputWidget.setValidator(input_validator)
        return inputWidget


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec_())
